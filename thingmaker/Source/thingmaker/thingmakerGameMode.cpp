// Copyright Epic Games, Inc. All Rights Reserved.

#include "thingmakerGameMode.h"
#include "thingmakerPlayerController.h"
#include "thingmakerCharacter.h"
#include "ThingGameState.h"
#include "UObject/ConstructorHelpers.h"

AthingmakerGameMode::AthingmakerGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AthingmakerPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}

void AthingmakerGameMode::StartPlay()
{
	Super::StartPlay();
}
