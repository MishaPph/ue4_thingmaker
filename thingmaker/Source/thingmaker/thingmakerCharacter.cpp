// Copyright Epic Games, Inc. All Rights Reserved.

#include "thingmakerCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "BaseThing.h"
#include "ITool.h"
#include "Components/BoxComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "Components/SceneComponent.h"
#include "BaseMachineThing.h"
#include "Blueprint/UserWidget.h"
#include "Components/PickableComponent.h"
#include "Components/InteractableComponent.h"

AthingmakerCharacter::AthingmakerCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;
	GetCharacterMovement()->SetJumpAllowed(false);
	
	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/TopDownCPP/Blueprints/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
	
	OverlapComp = CreateDefaultSubobject<UBoxComponent>(TEXT("OverLapComp"));
	OverlapComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	OverlapComp->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	OverlapComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	OverlapComp->SetBoxExtent(FVector(200.0f));
	OverlapComp->OnComponentBeginOverlap.AddDynamic(this, &AthingmakerCharacter::OnComponentOverlapBegin);
	OverlapComp->OnComponentEndOverlap.AddDynamic(this, &AthingmakerCharacter::OnComponentOverlapEnd);
	OverlapComp->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	GrabLocation = CreateDefaultSubobject<USceneComponent>("GrabLocation");
	GrabLocation->SetupAttachment(RootComponent);

	PhysicsHandle = CreateDefaultSubobject<UPhysicsHandleComponent>("PhysicsHandle");
	DashImpulse = 20;
}

void AthingmakerCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void AthingmakerCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &AthingmakerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AthingmakerCharacter::MoveRight);
	PlayerInputComponent->BindAction(TEXT("Pick"), IE_Released, this, &AthingmakerCharacter::Action_Pick_Released);
	PlayerInputComponent->BindAction(TEXT("Do"), IE_Pressed, this, &AthingmakerCharacter::Action_Do_Pressed);
	PlayerInputComponent->BindAction(TEXT("Do"), IE_Released, this, &AthingmakerCharacter::Action_Do_Released);
	PlayerInputComponent->BindAction(TEXT("Dash"), IE_Released, this, &AthingmakerCharacter::Dash);
}


void AthingmakerCharacter::MoveForward(float Value)
{
	if ((Controller != nullptr) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AthingmakerCharacter::Grab_Thing(UPickableComponent* Thing)
{
	if(Thing == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Grab_Thing Null"));
		return;
	}
	UE_LOG(LogTemp, Log, TEXT("Grab_Thing %s"), *Thing->GetName());
	Picked = Thing;
	Picked->Thing->SetCollisionEnabled(false);
	Picked->Pick(PhysicsHandle, GrabLocation);
}

bool AthingmakerCharacter::TryInteractWith(UInteractableComponent* Thing)
{
	if (Thing->CouldBeAdded(Picked))
	{
		UE_LOG(LogTemp, Warning, TEXT("Add %s to %s"), *(Picked->GetOwner()->GetName()), *(Thing->GetOwner()->GetName()));
		Thing->Add(Picked);
		return true;
	}

	if (Thing->CanReceiveThingOnTop(Picked))
	{
		UE_LOG(LogTemp, Warning, TEXT("Place %s on top of %s"), *(Picked->GetOwner()->GetName()), *(Thing->GetOwner()->GetName()));
		Thing->AddObjectOnTop(Picked);
		return true;
	}

	UE_LOG(LogTemp, Error, TEXT("Add %s to %s"), *(Picked->GetOwner()->GetName()), *(Thing->GetOwner()->GetName()));
	//can put on?
	//can insert
	//can take
	/*if(Thing->GetChild() != nullptr)
	{
		return Combine(Thing->GetChild());
	}
	if(Thing->bAllowAddOnTop)
	{
		if(Thing->TryAdd(Picked))
		{
			UE_LOG(LogTemp, Warning, TEXT("Picked Released TryAdd %s"), *(Thing->GetName()));
			Picked = nullptr;
			return true;
		}
	} else if(Thing->TryInsert(Picked))
	{
		UE_LOG(LogTemp, Warning, TEXT("Picked Released TryInsert %s to %s"), *(Picked->GetName()), *(Thing->GetName()));
		Picked = nullptr;
		return true;
	} else if(Picked->TryInsert(Thing))
	{
		return true;
	}*/
	return false;
}

void AthingmakerCharacter::Action_Pick_Released()
{
	if(Picked == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Picked %d"), Triggered.Num());
		if(Triggered.Num() > 0)
		{
			//can pick up the object
			for (auto Element : Triggered)
			{
				UE_LOG(LogTemp, Warning, TEXT("Interact with %s"), *(Element->GetOwner()->GetName()));
				auto pick = Cast<UPickableComponent>(Element->GetOwner()->GetComponentByClass(UPickableComponent::StaticClass()));
				if (pick != nullptr && pick->CanPickUp())
				{
					Grab_Thing(pick);
					break;
				}
			}

			//can spawn  object
			for (auto Element : Triggered)
			{
				UE_LOG(LogTemp, Warning, TEXT("Try Get From %s"), *(Element->GetOwner()->GetName()));
				if (Element->CanSpawn())
				{
					auto Thing = Element->SpawnThing();
					
					if (Thing != nullptr) {
						auto pick = Cast<UPickableComponent>(Thing->GetComponentByClass(UPickableComponent::StaticClass()));

						if (pick == nullptr) {
							UE_LOG(LogTemp, Error, TEXT("Spawned from %s is not pickable"), *(Element->GetOwner()->GetName()));
							continue;
						}
						Grab_Thing(pick);
						break;
					}
				}
			}
		}
	} else if(Triggered.Num() > 0)
	{
		for (int i = 1; i <= Triggered.Num(); i++)
		{
			if(TryInteractWith(Triggered[Triggered.Num() - i]))
			{
				Picked->Thing->SetCollisionEnabled(true);
				Picked = nullptr;
				break;
			}
		}
	} else
	{
		UE_LOG(LogTemp, Warning, TEXT("Picked Drop"));
		Picked->Drop();
		Picked->Thing->SetCollisionEnabled(true);
		Picked = nullptr;
	}
}

void AthingmakerCharacter::Action_Do_Pressed()
{
	DoPressed = true;
}

void AthingmakerCharacter::Action_Do_Released()
{
	DoPressed = false;
}

void AthingmakerCharacter::Dash()
{
	LaunchCharacter(GetActorForwardVector()*DashImpulse, true, false);
}

void AthingmakerCharacter::MoveRight(float Value)
{
	if ( (Controller != nullptr) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
		FRotator const ControlSpaceRot = Controller->GetControlRotation();

		auto direction = FRotationMatrix(ControlSpaceRot).GetScaledAxis( EAxis::Y );
		// transform to world space and add it
		AddMovementInput( FRotationMatrix(ControlSpaceRot).GetScaledAxis( EAxis::Y ), Value );

		UPawnMovementComponent* MovementComponent = GetMovementComponent();
		if (MovementComponent)
		{
			MovementComponent->AddInputVector(direction * Value, false);
		}
		else
		{
			Internal_AddMovementInput(direction * Value, false);
		}
	}
}


void AthingmakerCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if(DoPressed)
	{
		if (Picked != nullptr)
		{
			UE_LOG(LogTemp, Warning, TEXT("You have picked item %s"), *(Picked->GetName()));
			return;
		}
		if (Triggered.Num() > 0)
		{
			for (int i = 0; i < Triggered.Num(); i++)
			{
				if (Triggered[i] == nullptr)
				{
					UE_LOG(LogTemp, Warning, TEXT("The trigger %d null"), i);
					continue;
				}
				Triggered[i]->Interact(DeltaSeconds);
			}
		}
	}
}

void AthingmakerCharacter::OnComponentOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	auto Thing = Cast<UInteractableComponent>(OtherActor->GetComponentByClass(UInteractableComponent::StaticClass()));
	if(Thing == nullptr)
	{
		return;
	}
	
	UE_LOG(LogTemp, Warning, TEXT("OnComponentOverlapBegin %s"), *(OtherActor->GetName()));
	if(Triggered.Contains(Thing))
	{
		UE_LOG(LogTemp, Warning, TEXT("Already contains"));
		return;
	}

	Triggered.Add(Thing);
	Triggered.Sort([](const UInteractableComponent& Lhs, const UInteractableComponent& RHS)  { return Lhs.Priority < RHS.Priority;});
	/*for (auto Element : Triggered)
	{
	UE_LOG(LogTemp, Warning, TEXT("Element %s"), *(Element->GetActorLocation().ToString()));
	}*/
	UE_LOG(LogTemp, Warning, TEXT("Thing  first %s Added %s"), *(Triggered[0]->GetOwner()->GetName()), *OtherActor->GetName());
}

void AthingmakerCharacter::OnComponentOverlapEnd(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
	//auto Thing = Cast<ABaseThing>(OtherActor);
	auto Thing = Cast<UInteractableComponent>(OtherActor->GetComponentByClass(UInteractableComponent::StaticClass()));

	UE_LOG(LogTemp, Warning, TEXT("OnComponentOverlapEnd %s"), *OtherActor->GetName());
	if(Thing != nullptr)
	{
		Triggered.Remove(Thing);
	}
}