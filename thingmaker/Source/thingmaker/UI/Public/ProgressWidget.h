// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ProgressWidget.generated.h"

/**
 * 
 */
UCLASS()
class THINGMAKER_API UProgressWidget : public UUserWidget
{
	GENERATED_BODY()

	protected:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UProgressBar* ProgressBar;
	
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UImage* Icon;
	
	class UProgressActorComponent* ActiveProgress;
	public:
	void SetProgress(const float Progress);
	void SetVisible(const bool IsVisible);
	
	UFUNCTION()
	void Refresh();
};
