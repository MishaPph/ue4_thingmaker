// Fill out your copyright notice in the Description page of Project Settings.

#include "ProgressWidget.h"
#include "Components/ProgressBar.h"
#include "ProgressActorComponent.h"

void UProgressWidget::SetProgress(const float Progress)
{
	//UE_LOG(LogTemp, Log, TEXT("SetProgress %f"), Progress);
	ProgressBar->SetPercent(Progress);
}

void UProgressWidget::SetVisible(const bool IsVisible)
{
//	UE_LOG(LogTemp, Log, TEXT("SetVisible %d"), IsVisible?1:0);
	ProgressBar->SetVisibility(IsVisible?ESlateVisibility::Visible:ESlateVisibility::Hidden);
	Refresh();
}

void UProgressWidget::Refresh()
{
	if(ActiveProgress)
	{
		ProgressBar->SetPercent(ActiveProgress->Progress);
	}
}