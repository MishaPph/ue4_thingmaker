// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "GameWidget.generated.h"

/**
 * 
 */
UCLASS()
class THINGMAKER_API UGameWidget : public UUserWidget
{
	GENERATED_BODY()

	protected:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UHorizontalBox* ReceiptsContainer;

	public:
	virtual bool Initialize() override;
	
	UPROPERTY(Category = Default, EditAnywhere, BlueprintReadWrite)
	TSubclassOf<UUserWidget> ItemWidgetClass;
};
