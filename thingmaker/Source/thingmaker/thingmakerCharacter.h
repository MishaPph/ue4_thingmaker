// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "thingmakerCharacter.generated.h"

class ABaseThing;
class UPickableComponent;
class UInteractableComponent;

UCLASS(Blueprintable)
class AthingmakerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AthingmakerCharacter();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	class USceneComponent* GrabLocation;
	
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	class UPhysicsHandleComponent* PhysicsHandle;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
	class UParticleSystem* Fx;
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Thing")
	float DashImpulse;
protected:
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	void Action_Pick_Released();

	void Action_Do_Pressed();

	void Action_Do_Released();

	void Dash();
	
	void Grab_Thing(UPickableComponent* Thing);

	bool TryInteractWith(UInteractableComponent* Thing);
	
	UPROPERTY(VisibleAnywhere, Category = "Components")
	class UBoxComponent* OverlapComp;
private:
	
	bool DoPressed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Thing", meta = (AllowPrivateAccess = "true"))
	TArray<UInteractableComponent*> Triggered;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Thing", meta = (AllowPrivateAccess = "true"))
	UPickableComponent* Picked;

	UFUNCTION()
	void OnComponentOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
    void OnComponentOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
};

