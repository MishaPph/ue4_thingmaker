// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/GameStateBase.h"
#include "Components/CookBookComponent.h"
#include "ThingGameState.generated.h"

/**
 * 
 */
UCLASS()
class THINGMAKER_API AThingGameState : public AGameStateBase
{
	GENERATED_BODY()

public:
	AThingGameState();

	virtual void Tick(float DeltaSeconds) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CookBook")
	UCookBookComponent* CookBook;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CookBook")
	class UThingInfo* ThingInfo;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "CookBook")
	FString Name;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CookBook")
	FString Name2;
};
