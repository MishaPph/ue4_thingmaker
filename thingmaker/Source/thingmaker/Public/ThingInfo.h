// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseThing.h"
#include "Engine/DataAsset.h"
#include "ThingInfo.generated.h"

USTRUCT(BlueprintType)
struct FItem
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UStaticMesh* Mesh;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	UTexture2D* Icon;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Value;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ABaseThing> ThingClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
	TEnumAsByte<EThingId> ThingTypeId;
};

/**
 * 
 */
UCLASS()
class THINGMAKER_API UThingInfo : public UDataAsset
{
	GENERATED_BODY()

	public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Damage;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<FItem> Items;

	FItem GetInfo(const EThingId ThingId);
};
