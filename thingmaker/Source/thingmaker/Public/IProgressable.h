// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "IProgressable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UIProgressable : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class THINGMAKER_API IIProgressable
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual void SetProgress(float Value) = 0;
	virtual float GetProgress() = 0;
	virtual void AddProgress(float Value) = 0;
};
