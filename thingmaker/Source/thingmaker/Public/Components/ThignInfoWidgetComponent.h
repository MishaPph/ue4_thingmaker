// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ThignInfoWidgetComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THINGMAKER_API UThignInfoWidgetComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UThignInfoWidgetComponent();

	UFUNCTION()
	void ProgressChanged();
	UFUNCTION()
	void ProgressFinished();
	UFUNCTION()
	void StartProgressChanged();
	
	class UProgressWidget* Widget;
	class UProgressActorComponent* ProgressComponent;
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	void InitWidget();

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	class UProgressActorComponent* GetProgressComponent() const { return  ProgressComponent;}

	void Added();
	void End();
};
