// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BaseThing.h"
#include "InteractableComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THINGMAKER_API UInteractableComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UInteractableComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Thing")
	class ABaseMachineThing* Machine;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Thing")
	bool AllowObjectOnTop;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Thing")
	TEnumAsByte<EThingType> RequiredType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Thing")
	TEnumAsByte<EThingState> RequiredState;
public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
	int Priority;

	bool CanSpawn();

	class ABaseThing* SpawnThing();

	bool CanReceiveThingOnTop(class UPickableComponent* Pickable);
	void AddObjectOnTop(class UPickableComponent* Pickable);
	bool CouldBeAdded(class UPickableComponent* Pickable);
	void Add(class UPickableComponent* Pickable);
	void Interact(float Delta);
};
