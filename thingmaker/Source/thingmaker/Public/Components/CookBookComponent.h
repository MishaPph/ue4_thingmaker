// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ReceiptData.h"
#include "CookBookComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCookBookUpdated);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class THINGMAKER_API UCookBookComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCookBookComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	
	void GenerateReceipts();
	void Complete(class UReceiptObject* ReceiptObject);

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void CompleteReceipt(TArray<FThingData> &Items);
	
	//UFUNCTION(BlueprintCallable, Category = "Receipt")
    bool AddReceipt(class UReceiptObject* ReceiptObject);
	
	//UFUNCTION(BlueprintCallable, Category = "Receipt")
	bool RemoveReceipt(class UReceiptObject* ReceiptObject);

	UPROPERTY(EditAnywhere, Category = "CookBook")
	TArray<class UReceiptData*> AllAvailableReceipt;

	UPROPERTY(BlueprintAssignable, Category = "CookBook")
	FOnCookBookUpdated OnCookBookUpdated;

	UPROPERTY(BlueprintAssignable, Category = "CookBook")
	FOnCookBookUpdated OnCookBookInitialized;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Items")
	TArray<class UReceiptObject*> Receipts;
};
