// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseThing.h"
#include <ThingInfo.h>

#include "BaseMachineThing.generated.h"

/**
 * 
 */
UCLASS()
class THINGMAKER_API ABaseMachineThing : public ABaseThing
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Thing")
	class USceneComponent* PickLocation;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Thing")
	TEnumAsByte<EThingId> DeliveryTypeId;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Thing")
	UThingInfo* ThingInfo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
	bool bAllowAddOnTop;

	virtual void Removed();
	virtual void Added();

	virtual ABaseThing* SpawnThing(TSubclassOf<ABaseThing> FromClass, const FVector Position);

	void DetachThing(ABaseThing* Thing);
public:
	ABaseMachineThing();
	virtual void BeginPlay() override;
	void AlignTopObject();

	virtual void RemoveThing(ABaseThing* Thing) override;

	virtual ABaseThing* CreateThing();
		
	virtual void AddThingOnTop(ABaseThing* Thing);
	
	bool HaveTopThing() const { return  Child != nullptr; };
};
