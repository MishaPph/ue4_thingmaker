// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "ITool.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UITool : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class THINGMAKER_API IITool
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	virtual void Do(float Step) = 0;

	virtual bool AllowManual() = 0;
};
