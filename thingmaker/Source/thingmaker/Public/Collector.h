// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseMachineThing.h"
#include "Collector.generated.h"

/**
 * 
 */
UCLASS()
class THINGMAKER_API ACollector : public ABaseMachineThing
{
	GENERATED_BODY()

	/* Handle to manage the timer */
	FTimerHandle SpawnTimerHandle;

	class APlate* ActivePlate;
	
	TArray<ABaseThing*> ActivePlates;
	
	void SpawnDirtyPlate();
public:

	virtual void AddThingOnTop(ABaseThing* Thing) override;
	virtual ABaseThing* CreateThing() override;

	UPROPERTY(EditAnyWhere)
	ABaseMachineThing* DirtyPlateMachine;
};
