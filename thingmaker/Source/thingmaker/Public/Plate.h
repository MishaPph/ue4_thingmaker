// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseThing.h"
#include "ThingReceiver.h"
#include "Plate.generated.h"

/**
 * 
 */
UCLASS()
class THINGMAKER_API APlate : public ABaseThing, public IThingReceiver
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, Category = "Thing")
	class USceneComponent* PickLocation;

	public:
	// Sets default values for this actor's properties
	APlate();
	
	virtual void BeginPlay() override;

	UFUNCTION()
	void SetClean();

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TArray<ABaseThing*> Items;

	bool IsEmpty() const {return Items.Num() == 0;};

	virtual bool CanReceive(class ABaseThing* Thing) override;
	virtual void Receive(class ABaseThing* Thing) override;

	bool CanAdd(EThingId ThingId, EThingState ThingState);
	bool TryInsert(ABaseThing* Thing);
	virtual bool AllowProcess(EProcessType ProcessType) override;
	
	void Collect();
};
