// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseMachineThing.h"
#include "SolidThing.h"
#include "ThingReceiver.h"
#include "Combiner.generated.h"

/**
 * 
 */
UCLASS()
class THINGMAKER_API ACombiner : public ABaseMachineThing, public IThingReceiver
{
	GENERATED_BODY()
private:
	EThingId ActiveClass;
protected:
	UPROPERTY(BlueprintReadOnly, Category = Status)
	int Count;
	
	class UThignInfoWidgetComponent* InfoWidget;
public:
	ACombiner();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
	int MaxCount;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
	bool OneType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
	TEnumAsByte<EThingState> RequiredState;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
	TEnumAsByte<EProcessType> SupportProcessType;

	EThingId ProduceThingId() const {return ActiveClass;}
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
	TEnumAsByte<EThingState> ProduceState;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
	TArray< TSubclassOf<ASolidThing> > Types;

	bool Completed() const
	{
		return Count == MaxCount;
	}

	virtual bool CanReceive(class ABaseThing* Thing) override;
	virtual void Receive(class ABaseThing* Thing) override;

	bool CanAdd(EThingId ThingId, EThingState ThingState);
	bool TryInsert(ABaseThing* thing);
	virtual void RemoveThing(ABaseThing* thing) override;
	virtual ABaseThing* CreateThing() override;
	
	virtual bool AllowProcess(EProcessType ProcessType) override;
};
