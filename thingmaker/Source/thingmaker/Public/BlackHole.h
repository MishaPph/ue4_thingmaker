// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseMachineThing.h"
#include "BlackHole.generated.h"

/**
 * 
 */
UCLASS()
class THINGMAKER_API ABlackHole : public ABaseMachineThing
{
	GENERATED_BODY()

	public:
	virtual void AddThingOnTop(ABaseThing* Thing) override;
};
