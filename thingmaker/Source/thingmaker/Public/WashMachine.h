// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ToolMachine.h"
#include "WashMachine.generated.h"

/**
 * 
 */
UCLASS()
class THINGMAKER_API AWashMachine : public AToolMachine
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(EditAnyWhere, BlueprintReadWrite)
	TArray<ABaseThing*> All;
	
	virtual bool AllowDo() override;
	virtual void Added() override;
	
public:
	
	UPROPERTY(EditAnyWhere, BlueprintReadWrite)
	ABaseMachineThing* OutMachine;

	virtual void BeginPlay() override;
	virtual void ProcessFinished() override;
};
