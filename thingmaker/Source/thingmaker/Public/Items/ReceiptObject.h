// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ReceiptData.h"
#include "ReceiptObject.generated.h"

/**
 * 
 */
UCLASS(BlueprintType, Blueprintable, EditInlineNew, DefaultToInstanced)
class THINGMAKER_API UReceiptObject : public UObject
{
	GENERATED_BODY()

	public:
	UReceiptObject();

	void SetData(UReceiptData* rData) { Data = rData; };
	UWorld* World;
	class UCookBookComponent* CoookBook;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Object")
	UReceiptData* Data;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Object")
	float Progress;
};
