// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseThing.generated.h"


UENUM()
enum EThingId
{
	Red     UMETA(DisplayName = "Cocoa"),
    Brown   UMETA(DisplayName = "Brown"),
    Plate   UMETA(DisplayName = "Plate"),
	Undefined   UMETA(DisplayName = "Undefined"),
};


UENUM(BlueprintType, meta=(Bitflags, UseEnumValuesAsMaskValuesInEditor = "true"))
enum class EThingType:uint8
{
	Undefined	= 0x00,
	Solid		= 0x01,
	Box			= 0x02,
	Plate		= 0x04,
	Combiner	= 0x08,
};
ENUM_CLASS_FLAGS(EThingType)

UENUM()
enum EThingState
{
	None     UMETA(DisplayName = "None"),
    Solid    UMETA(DisplayName = "Solid"),
    Melted   UMETA(DisplayName = "Melted"),
    Dirty    UMETA(DisplayName = "Dirty"),
    Boiled   UMETA(DisplayName = "Boiled"),
	Filled   UMETA(DisplayName = "Filled"),
};


UENUM()
enum EProcessType
{
	Boil   UMETA(DisplayName = "Boil"),
    Chop   UMETA(DisplayName = "Chop"),
    Wash   UMETA(DisplayName = "Wash"),
};

UCLASS()
class THINGMAKER_API ABaseThing : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseThing();

protected:

	UPROPERTY(EditInstanceOnly, Category = "Thing")
	UStaticMeshComponent* MeshComponent;

	UPROPERTY(BlueprintReadOnly, Category = "Thing")
	ABaseThing* Parent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Thing")
	ABaseThing* Child;
public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
	TEnumAsByte<EThingId> ThingTypeId;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
	TEnumAsByte<EThingType> ThingType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
	TEnumAsByte<EThingState> State;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
	bool Dropped;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
	FVector AttachOffset;

	ABaseThing*  GetChild() const { return Child; }
	
	ABaseThing*  GetParent() const { return Parent; };

	virtual void RemoveThing(ABaseThing* Thing);
	
	virtual void ChangeParent(ABaseThing* Thing);

	virtual bool TryAdd(ABaseThing* Thing);

	UFUNCTION()
	virtual void SetState(EThingState NewState);
	
	class UPrimitiveComponent* GetPrimitiveComponent();
	
	virtual void Pick();
	
	virtual void Drop();
	
	virtual bool AllowProcess(EProcessType ProcessType);

	virtual void ToggleHidden(bool NewbHidden);

	virtual void Remove();

	void SetCollisionEnabled(bool enable);
};
