// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseThing.h"

#include "SolidThing.generated.h"

/**
 * 
 */
UCLASS()
class THINGMAKER_API ASolidThing : public ABaseThing
{
	GENERATED_BODY()

protected:

	UPROPERTY(VisibleAnywhere, Category = "Thing")
	class USphereComponent *SphereComp;

	UPROPERTY(EditDefaultsOnly, Category = "Thing")
	TSubclassOf<AActor> CommonClass;

	UPROPERTY(EditDefaultsOnly, Category = "Thing")
	TSubclassOf<AActor> CompleteClass;

	AActor* ThingInstance;

	class UWidgetComponent* Widget;
	virtual void BeginPlay() override;
public:
	// Sets default values for this actor's properties
	ASolidThing();
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mesh")
	UStaticMeshComponent* ReadyMeshComponent;

	virtual void SetState(EThingState NewState) override;
	
	virtual bool AllowProcess(EProcessType ProcessType) override;
	
	virtual void Remove() override;
private:
	void Spawn(TSubclassOf<AActor> RefClass);
};
