// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseMachineThing.h"
#include "ITool.h"
#include "ToolMachine.generated.h"

class UProgressWidget;
/**
 * 
 */
UCLASS()
class THINGMAKER_API AToolMachine : public ABaseMachineThing, public IITool
{
	GENERATED_BODY()
private:

	UPROPERTY(EditDefaultsOnly, Category = "Thing")
	TSubclassOf<UProgressWidget> BP_Progress;
	
protected:
	UProgressWidget* Widget;
	class UProgressActorComponent* ProgressComponent;
	virtual void Removed() override;
	virtual void Added() override;

	virtual bool AllowDo();
public:

	AToolMachine();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
	TEnumAsByte<EProcessType> ProcessType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
	TEnumAsByte<EThingState> ToState;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
	float Speed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
	bool Auto;
	
	virtual void Do(float Step) override;
	
	virtual void ProcessFinished();

	virtual bool AllowManual() override;

	virtual void Tick(float DeltaTime) override;

};
