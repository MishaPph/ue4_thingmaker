// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "IProgressable.h"
#include "Components/ActorComponent.h"
#include "ProgressActorComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnProgressChangedCB);
	
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THINGMAKER_API UProgressActorComponent : public UActorComponent, public IIProgressable
{
	GENERATED_BODY()
	private:
	bool Ready;
public:	
	// Sets default values for this component's properties
	UProgressActorComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	float Progress;
	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	virtual float GetProgress() override;
	virtual void AddProgress(float Value) override;
	virtual void SetProgress(float Value) override;

	void Reset();
	
	bool IsReady() const {return Ready;};
	
	UPROPERTY(BlueprintAssignable, Category="Progress")
	FOnProgressChangedCB OnProgressChanged;

	UPROPERTY(BlueprintAssignable, Category="Progress")
	FOnProgressChangedCB OnProgressFinish;

	UPROPERTY(BlueprintAssignable, Category="Progress")
	FOnProgressChangedCB OnStartProgress;
};
