// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "BaseThing.h"
#include "ReceiptData.generated.h"


USTRUCT(BlueprintType)
struct FThingData
{
	GENERATED_USTRUCT_BODY()

    UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TEnumAsByte<EThingId> Id;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TEnumAsByte<EThingState> State;

	bool operator==(const FThingData& Rhs) const{ 
		return Id == Rhs.Id && State == Rhs.State;
	}
};

/**
 * 
 */
UCLASS()
class THINGMAKER_API UReceiptData : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, BlueprintReadOnly)
	TArray<FThingData> Items;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, BlueprintReadOnly)
	FString Name;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, BlueprintReadOnly)
	float TimeToFinish;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	int Reward = 10;
	
	float GetCompletePercent(TArray<FThingData> &ComparateItems);
	
};
