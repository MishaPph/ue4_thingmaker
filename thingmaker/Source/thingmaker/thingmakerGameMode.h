// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "thingmakerGameMode.generated.h"

UCLASS(minimalapi)
class AthingmakerGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AthingmakerGameMode();

	virtual void StartPlay() override;
};



