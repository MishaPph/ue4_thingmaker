// Copyright Epic Games, Inc. All Rights Reserved.

#include "thingmaker.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, thingmaker, "thingmaker" );

DEFINE_LOG_CATEGORY(Logthingmaker)
 