// Fill out your copyright notice in the Description page of Project Settings.

#include "Components/PickableComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include <BaseThing.h>

// Sets default values for this component's properties
UPickableComponent::UPickableComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}


// Called when the game starts
void UPickableComponent::BeginPlay()
{
	Super::BeginPlay();
	Thing = Cast<ABaseThing>(GetOwner());
}

void UPickableComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

bool UPickableComponent::CanPickUp()
{
	return true;
}

void UPickableComponent::Drop()
{
	GetOwner()->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
	Thing->Drop();
}

void UPickableComponent::Pick(UPhysicsHandleComponent* PhysicsHandle, USceneComponent* GrabLocation) {
	
	Thing->Pick();

	auto mesh = Thing->GetPrimitiveComponent();
	PhysicsHandle->GrabComponentAtLocation(mesh, TEXT(""), GrabLocation->GetComponentLocation());
	GetOwner()->AttachToComponent(GrabLocation, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	
	if (Thing->GetParent() != nullptr)
	{
		Thing->GetParent()->RemoveThing(Thing);
	}
}

