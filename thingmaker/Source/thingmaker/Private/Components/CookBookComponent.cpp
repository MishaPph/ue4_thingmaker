// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/CookBookComponent.h"
#include "Items/ReceiptObject.h"
#include "ReceiptData.h"

// Sets default values for this component's properties
UCookBookComponent::UCookBookComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UCookBookComponent::BeginPlay()
{
	Super::BeginPlay();
	
	GenerateReceipts();
}


// Called every frame
void UCookBookComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	for (auto Element : Receipts)
	{
		Element->Progress += DeltaTime/Element->Data->TimeToFinish;
		if(Element->Progress >= 1.0f)
		{
			Complete(Element);
			break;
		}
	}
}

bool UCookBookComponent::AddReceipt(UReceiptObject* ReceiptObject)
{
	if(ReceiptObject == nullptr)
		return false;
	
	//UE_LOG(LogTemp, Warning, TEXT("AddReceipt"));
	
	ReceiptObject->CoookBook = this;
	ReceiptObject->World = GetWorld();
	Receipts.Add(ReceiptObject);
	OnCookBookUpdated.Broadcast();
	return true;
}

bool UCookBookComponent::RemoveReceipt(UReceiptObject* ReceiptObject)
{
	//UE_LOG(LogTemp, Warning, TEXT("RemoveReceipt"));
	if(Receipts.Remove(ReceiptObject) != 0)
	{
		OnCookBookUpdated.Broadcast();
		return true;
	} 
	return false;
}


void UCookBookComponent::GenerateReceipts()
{
	if(AllAvailableReceipt.Num() == 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("GenerateReceipts IsEmpty"));
		return;
	}
	const auto Index = FMath::RandRange(0, AllAvailableReceipt.Num() - 1);
	const auto MyObj = NewObject<UReceiptObject>(this);
	MyObj->Progress = 0.0f;
	MyObj->SetData(AllAvailableReceipt[Index]);
	//UE_LOG(LogTemp, Warning, TEXT("GenerateReceipts %f"), MyObj->Progress);
	AddReceipt(MyObj);
}

void UCookBookComponent::CompleteReceipt(TArray<FThingData> &Items)
{
	//UE_LOG(LogTemp, Warning, TEXT("CompleteReceipt"));
	UReceiptObject* MostSuccess = nullptr;
	float MaxValue = 0;
	for (auto CItem : Items)
	{
		UE_LOG(LogTemp, Warning, TEXT("CItem %d "), CItem.Id.GetValue());
	}
	
	for (auto Element : Receipts)
	{
		const auto V = Element->Data->GetCompletePercent(Items);
		UE_LOG(LogTemp, Warning, TEXT("GetCompletePercent %f"), V);
		if(V > MaxValue)
		{
			MaxValue = V;
			MostSuccess = Element;
		}
	}
	if(MostSuccess != nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("MostSuccess %f"), MaxValue);
		Complete(MostSuccess);
	}
}

void UCookBookComponent::Complete(UReceiptObject* ReceiptObject)
{
	RemoveReceipt(ReceiptObject);
	if(Receipts.Num() == 0)
		GenerateReceipts();
}
