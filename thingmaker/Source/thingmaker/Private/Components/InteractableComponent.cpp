 // Fill out your copyright notice in the Description page of Project Settings.
#include "Components/InteractableComponent.h"
#include "Components/PickableComponent.h"
#include "BaseThing.h"
#include "BaseMachineThing.h"
#include "ITool.h"
#include "ThingReceiver.h"

// Sets default values for this component's properties
UInteractableComponent::UInteractableComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	RequiredState = EThingState::None;
	RequiredType = EThingType::Undefined;
	AllowObjectOnTop = true;
}

// Called when the game starts
void UInteractableComponent::BeginPlay()
{
	Super::BeginPlay();
	Machine = Cast<ABaseMachineThing>(GetOwner());
}

// Called every frame
void UInteractableComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

bool UInteractableComponent::CanSpawn()
{
	return Machine != nullptr;
}

ABaseThing* UInteractableComponent::SpawnThing()
{
	if (Machine->GetChild() != nullptr)
	{
		return Machine->GetChild();
	}
	return Machine->CreateThing();
}

bool UInteractableComponent::CanReceiveThingOnTop(UPickableComponent* Pickable)
{
	if (!AllowObjectOnTop)
		return false;

	if (Machine == nullptr)
	{
		UE_LOG(LogTemp, Log, TEXT("No Machine"));
		return false;
	}
	if (Machine->GetChild() != nullptr)
	{
		UE_LOG(LogTemp, Log, TEXT("The Machine %s has a child %s"), *(Machine->GetName()), *(Machine->GetChild()->GetName()));
		return false;
	}

	if (RequiredType != EThingType::Undefined)
	{
		if (RequiredType == Pickable->Thing->ThingType)
		{
			return RequiredState == Pickable->Thing->State;
		}
		return false;
	}

	return true;
}

void UInteractableComponent::AddObjectOnTop(UPickableComponent* Pickable)
{
	Machine->AddThingOnTop(Pickable->Thing);
}

bool UInteractableComponent::CouldBeAdded(UPickableComponent* Pickable)
{
	auto receiver = Cast<IThingReceiver>(GetOwner());

	if (receiver == nullptr)
	{
		UE_LOG(LogTemp, Log, TEXT("No receiver %s "), *(GetOwner()->GetName()));
		return false;
	}

	if (RequiredType != EThingType::Undefined)
	{
		UE_LOG(LogTemp, Log, TEXT("Try to add Type : %s State : %s"), *(UEnum::GetValueAsString(Pickable->Thing->ThingType)), *(UEnum::GetValueAsString(Pickable->Thing->State)));
		if (RequiredType == Pickable->Thing->ThingType)
		{
			return RequiredState == Pickable->Thing->State && receiver->CanReceive(Pickable->Thing);
		}
		return false;
	}

	return receiver->CanReceive(Pickable->Thing);
}

void UInteractableComponent::Add(UPickableComponent* Pickable)
{
	auto receiver = Cast<IThingReceiver>(GetOwner());

	if (receiver == nullptr)
	{
		UE_LOG(LogTemp, Log, TEXT("No receiver %s "), *(GetOwner()->GetName()));
		return;
	}
	receiver->Receive(Pickable->Thing);
}

void UInteractableComponent::Interact(float DeltaSeconds)
{
	auto Tool = Cast<IITool>(Machine);
	if (Tool && Tool->AllowManual())
	{
		Tool->Do(DeltaSeconds);
	}
}
