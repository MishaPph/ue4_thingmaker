// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/ThignInfoWidgetComponent.h"


#include "ProgressActorComponent.h"
#include "Components/WidgetComponent.h"
#include "thingmaker/UI/Public/ProgressWidget.h"
// Sets default values for this component's properties
UThignInfoWidgetComponent::UThignInfoWidgetComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UThignInfoWidgetComponent::StartProgressChanged()
{
	Widget->SetVisible(true);
}

void UThignInfoWidgetComponent::ProgressChanged()
{
	Widget->SetProgress(ProgressComponent->Progress);
}

void UThignInfoWidgetComponent::ProgressFinished()
{
	Widget->SetVisible(false);
	
}
// Called when the game starts
void UThignInfoWidgetComponent::BeginPlay()
{
	Super::BeginPlay();

	if (Widget != nullptr)
		return;
		
	InitWidget();
	
	ProgressComponent = Cast<UProgressActorComponent>(GetOwner()->GetComponentByClass(UProgressActorComponent::StaticClass()));
	if(ProgressComponent != nullptr)
	{
		ProgressComponent->InitializeComponent();
		//	UE_LOG(LogTemp, Warning, TEXT("ProgressComponent Link %s"), *(Widget->GetClass()->GetName()));
		//	Widget->LinkProgressbar(ProgressComponent);
		ProgressComponent->OnProgressChanged.AddDynamic(this, &UThignInfoWidgetComponent::ProgressChanged);
		ProgressComponent->OnProgressFinish.AddDynamic(this, &UThignInfoWidgetComponent::ProgressFinished);
		ProgressComponent->OnStartProgress.AddDynamic(this, &UThignInfoWidgetComponent::StartProgressChanged);
	}
}


void UThignInfoWidgetComponent::InitWidget()
{
	auto widget = Cast<UWidgetComponent>(GetOwner()->GetComponentByClass(UWidgetComponent::StaticClass()));
	
	if(widget == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("UWidgetComponent Null"));
		return;
	}
	widget->InitWidget();
	if(!widget->GetUserWidgetObject())
	{
		UE_LOG(LogTemp, Error, TEXT("GetUserWidgetObject Null"));
		return;
	}
	
	Widget = Cast<UProgressWidget>(widget->GetUserWidgetObject());
	
	if(Widget == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Widget Null"));
	} else
	{
		Widget->SetVisible(false);
	}
}

// Called every frame
void UThignInfoWidgetComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UThignInfoWidgetComponent::Added()
{
	float P = FMath::Min(0.5f, GetProgressComponent()->Progress*0.5f);
	GetProgressComponent()->Reset();
	UE_LOG(LogTemp, Error, TEXT("Added start from %f"), P);
	GetProgressComponent()->AddProgress(P);
}

void UThignInfoWidgetComponent::End()
{
	ProgressComponent->Reset();
	if(Widget == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("Widget Null"));
	} else
	{
		Widget->SetVisible(false);
	}
}

