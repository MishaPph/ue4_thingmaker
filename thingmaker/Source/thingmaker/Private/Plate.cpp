// Fill out your copyright notice in the Description page of Project Settings.


#include "Plate.h"
#include "ProgressActorComponent.h"
#include "Combiner.h"

APlate::APlate()
{
	PickLocation = CreateDefaultSubobject<USceneComponent>("GrabLocation");
	PickLocation->SetupAttachment(RootComponent);
}

void APlate::BeginPlay()
{
	Super::BeginPlay();
	auto ProgressComponent = Cast<UProgressActorComponent>(GetComponentByClass(UProgressActorComponent::StaticClass()));
	if(ProgressComponent != nullptr)
	{
		ProgressComponent->OnProgressFinish.AddDynamic(this, &APlate::SetClean);
	} else
	{
		UE_LOG(LogTemp, Warning, TEXT("BeginPlay Plate"));
	}
}

void APlate::SetClean()
{
	State = None;
	UE_LOG(LogTemp, Warning, TEXT("SetClean Plate"));
}

bool APlate::CanReceive(ABaseThing* Thing)
{
	return CanAdd(Thing->ThingTypeId, Thing->State);
}

void APlate::Receive(ABaseThing* Thing)
{
	TryInsert(Thing);
}

bool APlate::CanAdd(EThingId ThingId, EThingState ThingState)
{
	if(State == Dirty)
	{
		UE_LOG(LogTemp, Warning, TEXT("Dirty Plate %s"), *GetName());
		return false;
	}
	if(ThingState != Boiled)
	{
		UE_LOG(LogTemp, Warning, TEXT("AddThing fail not Boiled"));
		return false;
	}
	
	for (auto Element : Items)
	{
		if(Element->ThingTypeId == ThingId)
		{
			UE_LOG(LogTemp, Warning, TEXT("AddThing fail not valid type %d"), ThingId);
			return false;
		}
	}
	return true;
}

bool APlate::TryInsert(ABaseThing* Thing)
{
	if(Thing == nullptr)
		return false;

	if(State == Dirty)
	{
		UE_LOG(LogTemp, Warning, TEXT("TryInsert  fail dirty")); 
		return false;
	}
	if(CanAdd(Thing->ThingTypeId, Thing->State))
	{
		State = EThingState::Filled;
		Thing->ChangeParent(this);
		Thing->AttachToComponent(PickLocation, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
		Items.Add(Thing);
		UE_LOG(LogTemp, Warning, TEXT("[Plate] add Items %s"), *UEnum::GetValueAsString(Thing->ThingTypeId));

		return true;
	}
	auto Combiner = Cast<ACombiner>(Thing);
	if(!Combiner)
		return false;
	UE_LOG(LogTemp, Warning, TEXT("TryInsert  %d"), Combiner->Completed()); 
	if(!Combiner->Completed())
	{
		UE_LOG(LogTemp, Warning, TEXT("TryInsert  not Completed")); 
		return false;
	}
	if(!CanAdd(Combiner->ProduceThingId(), Combiner->ProduceState))
	{
		UE_LOG(LogTemp, Warning, TEXT("TryInsert  Can't add %s %s"),  *UEnum::GetValueAsString(Combiner->ProduceThingId()), *UEnum::GetValueAsString(Combiner->ProduceState)); 
		return false;
	}
	const auto T = Combiner->CreateThing();
	if(T == nullptr)
	{
		return false;
	}

	State = EThingState::Filled;
	T->ChangeParent(this);
	T->AttachToComponent(PickLocation, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	Items.Add(T);
	UE_LOG(LogTemp, Warning, TEXT("[Plate] add Items %s"), *UEnum::GetValueAsString(T->ThingTypeId));
	Combiner->RemoveThing(this);

	return false;
}

void APlate::Collect()
{
	State = EThingState::Dirty;
	ToggleHidden(true);
	for (auto Element : Items)
	{
		Element->Destroy();
	}
	Items.Empty();
}

bool APlate::AllowProcess(EProcessType ProcessType)
{
	return State == Dirty && ProcessType == Wash;
}