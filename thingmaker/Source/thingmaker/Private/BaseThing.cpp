// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseThing.h"
#include "Components/BoxComponent.h"

// Sets default values
ABaseThing::ABaseThing()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));

	if (!ensure(MeshComponent != nullptr)) return;
	
	MeshComponent->CastShadow = false;
	MeshComponent->SetupAttachment(RootComponent, "GripPoint");
	RootComponent = MeshComponent;
	Dropped = true;
	ThingType = EThingType::Undefined;
}

void ABaseThing::RemoveThing(ABaseThing* Thing)
{
}

void ABaseThing::ChangeParent(ABaseThing* Thing)
{
	if(Parent != nullptr)
	{
		Parent->Child = nullptr;
	}
	Parent = Thing;
}

bool ABaseThing::TryAdd(ABaseThing* Thing)
{
/*	auto th = Thing->GetThing();
	if(th == nullptr)
	{
		th = Thing;
	}
	if(th != nullptr && CanAdd(th->ThingTypeId, th->State))
	{
		AddThing(th);
		Thing->RemoveThing(th);
		return true;
	}*/
	return false;
}

void ABaseThing::SetState(const EThingState NewState)
{
	State = NewState;
}

UPrimitiveComponent* ABaseThing::GetPrimitiveComponent()
{
	return MeshComponent;
}

void ABaseThing::Pick()
{
	UE_LOG(LogTemp, Log, TEXT("ABaseThing Pick %s"), *GetName());
	MeshComponent->SetSimulatePhysics(false);
	Dropped = false;
}

void ABaseThing::Drop()
{
	UE_LOG(LogTemp, Log, TEXT("ABaseThing Drop %s"), *GetName());
	Dropped = true;
	MeshComponent->SetSimulatePhysics(true);
}

bool ABaseThing::AllowProcess(EProcessType ProcessType)
{
	return true;
}

void ABaseThing::ToggleHidden(bool NewbHidden)
{
	this->SetActorHiddenInGame(NewbHidden);
}

void ABaseThing::Remove()
{
	GetWorld()->DestroyActor(this);
}

void ABaseThing::SetCollisionEnabled(bool enable)
{
	if (enable) {
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	}
	else {
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}
