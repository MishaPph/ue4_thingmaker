// Fill out your copyright notice in the Description page of Project Settings.

#include "WashMachine.h"
#include "ProgressActorComponent.h"
#include "thingmaker/UI/Public/ProgressWidget.h"


bool AWashMachine::AllowDo()
{
	return All.Num() > 0;
}

void AWashMachine::BeginPlay()
{
	Super::BeginPlay();
	ProgressComponent = Cast<UProgressActorComponent>(GetComponentByClass(UProgressActorComponent::StaticClass()));
}

void AWashMachine::Added()
{
	UE_LOG(LogTemp, Warning, TEXT("AWashMachine  Added"));
	if(Widget)
	{
		Widget->SetVisible(true);
	}
	Child->Pick();
	All.Add(Child);
}

void AWashMachine::ProcessFinished()
{
	UE_LOG(LogTemp, Warning, TEXT("ProcessFinished"));
	if(Widget)
	{
		Widget->SetVisible(false);
	}
	All[0]->Drop();
	All[0]->SetState(ToState);
	if(OutMachine != nullptr)
	{
		if(OutMachine->TryAdd(All[0]))
		{
			ProgressComponent->Reset();
		}
	}
	ProgressComponent->Reset();
	DetachThing(All[0]);
	All.RemoveAt(0);
	Removed();
}