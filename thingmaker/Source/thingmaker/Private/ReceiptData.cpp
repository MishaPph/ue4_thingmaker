// Fill out your copyright notice in the Description page of Project Settings.


#include "ReceiptData.h"

float UReceiptData::GetCompletePercent(TArray<FThingData> &ComparateItems)
{
	float Sum = 0;
	const auto Size = (Items.Num() + ComparateItems.Num())*0.5f;

	for (auto Element : Items)
	{
		for (auto CItem : ComparateItems)
		{
			UE_LOG(LogTemp, Warning, TEXT("CItem %s Element %s"), *UEnum::GetValueAsString(CItem.Id), *UEnum::GetValueAsString(Element.Id));
			if(CItem.Id == Element.Id)
			{
				Sum += 1.0f;
				ComparateItems.Remove(CItem);
				break;
			}
		}
	}
	return Sum/Size;
}
