// Fill out your copyright notice in the Description page of Project Settings.

#include "ToolMachine.h"
#include "SolidThing.h"
#include "Blueprint/UserWidget.h"
#include "ProgressActorComponent.h"

AToolMachine::AToolMachine()
{
	Speed = 0.01f;
}

void AToolMachine::Removed()
{
	Super::Removed();
}

void AToolMachine::Added()
{
	Super::Added();
	ProgressComponent = Cast<UProgressActorComponent>(Child->GetComponentByClass(UProgressActorComponent::StaticClass()));
	if (ProgressComponent == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("AToolMachine ProgressComponent not found in %s"), *Child->GetName());
	}
	else 
	{
		UE_LOG(LogTemp, Log, TEXT("AToolMachine Added  %s"), *Child->GetName());
	}
}

void AToolMachine::Do(float Step)
{
	if(ProgressComponent == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("AToolMachine Fail do"));
		return;
	}
	if(AllowDo())
	{
		//UE_LOG(LogTemp, Log, TEXT("AToolMachine AddProgress  %s"), *Child->GetName());
		ProgressComponent->AddProgress(Speed * Step);
		if(ProgressComponent->IsReady())
		{
			ProcessFinished();
		}
	} else if(!Auto)
	{
	//	UE_LOG(LogTemp, Error, TEXT("AToolMachine Fail AllowDo"));
	}
}

bool AToolMachine::AllowDo()
{
	return Child != nullptr && Child->AllowProcess(ProcessType);
}

void AToolMachine::ProcessFinished()
{
	Child->SetState(ToState);
}

bool AToolMachine::AllowManual()
{
	return !Auto;
}

void AToolMachine::Tick(float DeltaTime)
{
	if(Auto && AllowDo())
	{
		Do(DeltaTime);
	}
}
