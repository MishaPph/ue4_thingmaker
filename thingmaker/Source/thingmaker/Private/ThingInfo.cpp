// Fill out your copyright notice in the Description page of Project Settings.


#include "ThingInfo.h"

FItem UThingInfo::GetInfo(const EThingId ThingId)
{
	for (auto Item : Items)
	{
		if (Item.ThingTypeId == ThingId)
			return Item;
	}
	UE_LOG(LogTemp, Warning, TEXT("ThingInfo GetInfo %s"), *UEnum::GetValueAsString(ThingId));
	return Items[0];
}
