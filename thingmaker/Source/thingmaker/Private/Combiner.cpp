// Fill out your copyright notice in the Description page of Project Settings.

#include "Combiner.h"
#include "ProgressActorComponent.h"
#include "Blueprint/UserWidget.h"
#include "Components/ThignInfoWidgetComponent.h"

ACombiner::ACombiner()
{
	MaxCount = 3;
}

void ACombiner::BeginPlay()
{
	Super::BeginPlay();
	Count = 0;
	InfoWidget = Cast<UThignInfoWidgetComponent>(GetComponentByClass(UThignInfoWidgetComponent::StaticClass()));
	if(InfoWidget == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("ACombiner BeginPlay ProgressComponent null"));
	}
}

void ACombiner::RemoveThing(ABaseThing* Thing)
{
	if(InfoWidget && InfoWidget->GetProgressComponent()->IsReady() && Thing->ThingTypeId == Plate)
	{
		UE_LOG(LogTemp, Warning, TEXT("[Combiner] RemoveThing reason %s"), *UEnum::GetValueAsString(Thing->ThingTypeId));
		Count = 0;
		InfoWidget->End();
		ActiveClass = Undefined;
		Removed();
	}
}

ABaseThing* ACombiner::CreateThing()
{
	if(!InfoWidget)
	{
		UE_LOG(LogTemp, Warning, TEXT("ACombiner failed CreateThing ProgressComponent null"));
		return nullptr;
	}

	if(InfoWidget->GetProgressComponent()->IsReady())
	{
		if (!ThingInfo)
		{
			UE_LOG(LogTemp, Error, TEXT("ThingInfo is not assigned %s"), *GetName());
			return nullptr;
		}
		auto deliveryClass = ThingInfo->GetInfo(DeliveryTypeId).ThingClass;

		UE_LOG(LogTemp, Warning, TEXT("ACombiner CreateThing"));
		return SpawnThing(deliveryClass, FVector::ZeroVector);
	}
	UE_LOG(LogTemp, Warning, TEXT("ACombiner failed CreateThing not ready %d %f"), InfoWidget->GetProgressComponent()->IsReady(), InfoWidget->GetProgressComponent()->Progress);
	return nullptr;
}

bool ACombiner::CanReceive(ABaseThing* Thing)
{
	return CanAdd(Thing->ThingTypeId, Thing->State);
}

void ACombiner::Receive(ABaseThing* Thing)
{
	TryInsert(Thing);
}

bool ACombiner::CanAdd(EThingId ThingId, EThingState ThingState)
{
	if(Completed())
	{
		UE_LOG(LogTemp, Warning, TEXT("ACombiner Full"));
		return false;
	}
	if(ActiveClass != Undefined && ActiveClass != ThingId)
	{
		UE_LOG(LogTemp, Warning, TEXT("ACombiner not valid class"));
		return false;
	}
	if(RequiredState != ThingState)
	{
		UE_LOG(LogTemp, Warning, TEXT("ACombiner Not valid state"));
		return false;
	}
	return true;
}

bool ACombiner::TryInsert(ABaseThing* Thing)
{
	if(Thing == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("ACombiner TryInsert null"));
		return false;
	}
	if(!CanAdd(Thing->ThingTypeId, Thing->State)) 
		return false;

	ActiveClass = Thing->ThingTypeId;
	Count++;
	UE_LOG(LogTemp, Warning, TEXT("[Combiner] AddThing Count:%d"), Count);

	Thing->AttachToComponent(PickLocation, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	Thing->Remove();

	if (InfoWidget)
	{
		InfoWidget->Added();
	}
	Added();
	return true;
}

bool ACombiner::AllowProcess(EProcessType ProcessType)
{
	return Count != 0 && ProcessType == SupportProcessType;
}