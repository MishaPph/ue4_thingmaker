// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseMachineThing.h"
#include "Components/BoxComponent.h"

ABaseMachineThing::ABaseMachineThing()
{
	PickLocation = CreateDefaultSubobject<USceneComponent>("GrabLocation");
	PickLocation->SetupAttachment(RootComponent);
}

void ABaseMachineThing::BeginPlay()
{
	Super::BeginPlay();
	AlignTopObject();
}

void ABaseMachineThing::AlignTopObject()
{
	//UE_LOG(LogTemp, Warning, TEXT("ABaseMachineThing AlignTopObject %s"), *(this->GetName()));
	FHitResult Hit;
	//FCollisionQueryParams TraceParams(FName(TEXT("")), false, GetOwner()); 
	GetWorld()->LineTraceSingleByObjectType(
        OUT Hit,
        GetActorLocation(),
        GetActorLocation() + FVector::UpVector*300,
        FCollisionObjectQueryParams(FCollisionObjectQueryParams::AllObjects)
    );

	// See what if anything has been hit and return what
	AActor* ActorHit = Hit.GetActor();
	if (ActorHit) {
		UE_LOG(LogTemp, Log, TEXT("Line trace has hit: %s"), *(ActorHit->GetName()));
		const auto Thing = Cast<ABaseThing>(ActorHit);
		if(!TryAdd(Thing))
		{
			UE_LOG(LogTemp, Error, TEXT("Can't add thing: %s"), *(ActorHit->GetName()));
		}
	}
	//UE_LOG(LogTemp, Error, TEXT("Line trace has no hit %f"), Hit.Distance);
}

void ABaseMachineThing::DetachThing(ABaseThing* Thing)
{
	UE_LOG(LogTemp, Warning, TEXT("ABaseMachineThing RemoveChild %s"), *(Thing->GetName()));
	Thing->ChangeParent(nullptr);
	Thing = nullptr;
	Removed();
}

void ABaseMachineThing::RemoveThing(ABaseThing* Thing)
{
	if(Child != nullptr)
		DetachThing(Child);
}

ABaseThing* ABaseMachineThing::CreateThing()
{
	if (!ThingInfo)
	{
		UE_LOG(LogTemp, Error, TEXT("ThingInfo is not assigned %s"), *GetName());
		return nullptr;
	}
	auto deliveryClass = ThingInfo->GetInfo(DeliveryTypeId).ThingClass;
	if (deliveryClass)
	{
		UE_LOG(LogTemp, Log, TEXT("CreateThing SpawnThing %s"), *GetName());
		return SpawnThing(deliveryClass, PickLocation->GetComponentLocation());
	}
	UE_LOG(LogTemp, Error, TEXT("DeliveryClass Null"));
	return nullptr;
}

ABaseThing* ABaseMachineThing::SpawnThing(const TSubclassOf<ABaseThing> FromClass, const FVector Position)
{
	const FRotator Rotation = GetActorRotation();
	FActorSpawnParameters ActorSpawnParams;
	ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
	return GetWorld()->SpawnActor<ABaseThing>(FromClass, Position, Rotation, ActorSpawnParams);
}

void ABaseMachineThing::Removed()
{
}

void ABaseMachineThing::Added()
{
}

void ABaseMachineThing::AddThingOnTop(ABaseThing* Thing)
{
	UE_LOG(LogTemp, Warning, TEXT("ABaseMachineThing AddThing %s"), *(Thing->GetName()));
	Child = Thing;
	Child->ChangeParent(this);
	Child->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);

	//Child->AttachToComponent(PickLocation, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	FVector old = Child->GetActorLocation();
	Child->SetActorLocation(PickLocation->GetComponentLocation() + Thing->AttachOffset);

	Child->AddActorLocalOffset(Thing->AttachOffset);
	FVector newLocation = Child->GetActorLocation();

	UE_LOG(LogTemp, Log, TEXT("AddThing Old %s new %s, offset %s"), *old.ToString(), *newLocation.ToString(), *Thing->AttachOffset.ToString());

	Added();
}