// Fill out your copyright notice in the Description page of Project Settings.


#include "ProgressActorComponent.h"

// Sets default values for this component's properties
UProgressActorComponent::UProgressActorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	Progress = 0;
	Ready = false;
	// ...
}

// Called when the game starts
void UProgressActorComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

// Called every frame
void UProgressActorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UProgressActorComponent::GetProgress()
{
	return Progress;
}

void UProgressActorComponent::AddProgress(float Value)
{
	if(Progress == 0)
	{
		OnStartProgress.Broadcast();
	}
	Progress += Value;
	OnProgressChanged.Broadcast();
	if(Progress >= 1.0f && !Ready)
	{
		Ready = true;
		OnProgressFinish.Broadcast();
	}
}

void UProgressActorComponent::SetProgress(float Value)
{
	Ready = Value >= 1.0f;
	Progress = Value;
	OnProgressChanged.Broadcast();
}

void UProgressActorComponent::Reset()
{
	Ready = false;
	Progress = 0;
}
