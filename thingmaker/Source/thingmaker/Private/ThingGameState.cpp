// Fill out your copyright notice in the Description page of Project Settings.

#include "ThingGameState.h"
#include "Components/CookBookComponent.h"

AThingGameState::AThingGameState()
{
	PrimaryActorTick.bCanEverTick = true;
	SetActorTickEnabled(true);

	Name = "Hello";
	Name2 = "Hello2";
	CookBook = CreateDefaultSubobject<UCookBookComponent>(TEXT("Cook Book Component"));
	AddOwnedComponent(CookBook);
}


void AThingGameState::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}
