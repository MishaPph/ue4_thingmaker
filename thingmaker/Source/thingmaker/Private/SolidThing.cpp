#include "SolidThing.h"
#include "Blueprint/UserWidget.h"
#include "Blueprint/WidgetTree.h"
#include "Components/WidgetComponent.h"

ASolidThing::ASolidThing() 
{
}

void ASolidThing::BeginPlay()
{
	Super::BeginPlay();
	if (CommonClass == nullptr) {
		UE_LOG(LogTemp, Warning, TEXT("CommonClass is nullptr in %s. Please updete your Blueprini"), *GetName());
		return;
	}

    /*for (auto Element : GetComponents())
	{
		UE_LOG(LogTemp, Warning, TEXT("Component  %s"), *(Element->GetName()));
	}*/

	Widget = Cast<UWidgetComponent>(GetComponentByClass(UWidgetComponent::StaticClass()));
	
	State = EThingState::Solid;
	Spawn(CommonClass);
}

void ASolidThing::SetState(EThingState NewState)
{
	Super::SetState(NewState);
	if(NewState == EThingState::Melted)
	{
		UE_LOG(LogTemp, Warning, TEXT("SetState Melted"));
		GetWorld()->DestroyActor(ThingInstance);
		
		if (CompleteClass == nullptr) {
			UE_LOG(LogTemp, Warning, TEXT("CompleteClass is nullptr in %s. Please updete your Blueprini"), *GetName());
			return;
		}
		Spawn(CompleteClass);
	}
}

bool ASolidThing::AllowProcess(EProcessType ProcessType)
{
	if(State == Solid)
	{
		return ProcessType == Chop;
	}
	return false;
}

void ASolidThing::Remove()
{
	if(ThingInstance != nullptr)
		GetWorld()->DestroyActor(ThingInstance);
	Super::Remove();
}

void ASolidThing::Spawn(TSubclassOf<AActor> RefClass)
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	ThingInstance = GetWorld()->SpawnActor<AActor>(RefClass, GetTransform(), SpawnParams);
	ThingInstance->AttachToActor(this, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	ThingInstance->SetActorRelativeLocation(FVector(0,0, -50));
}