// Fill out your copyright notice in the Description page of Project Settings.

#include "BlackHole.h"

void ABlackHole::AddThingOnTop(ABaseThing* Thing)
{
	if(Thing == nullptr)
	{
		return;
	}
	Thing->Remove();
}
