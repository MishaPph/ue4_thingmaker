// Fill out your copyright notice in the Description page of Project Settings.


#include "Collector.h"
#include "Plate.h"
#include "ReceiptData.h"
#include "ThingGameState.h"
#include "Components/CookBookComponent.h"


void ACollector::AddThingOnTop(ABaseThing* Thing)
{
	auto Plate = Cast<APlate>(Thing);
	
	TArray<FThingData> Array;
	for (auto Element : Plate->Items)
	{
		UE_LOG(LogTemp, Warning, TEXT("Plate Items %d"), Element->ThingTypeId.GetValue());
		auto Item = FThingData();
		Item.Id = Element->ThingTypeId;
		Item.State = Element->State;
		Array.Add(Item);
		Element->Destroy();
	}
	
	//Complete receipt
	Plate->Collect();

	auto GameState = GetWorld()->GetGameState<AThingGameState>();

	if(GameState != nullptr)
	{
		GameState->CookBook->CompleteReceipt(Array);
	} else
	{
		UE_LOG(LogTemp, Warning, TEXT("GameState not found"));
	}

	ActivePlate = Plate;
	GetWorld()->GetTimerManager().SetTimer(SpawnTimerHandle, this, &ACollector::SpawnDirtyPlate, 3.0f, false);
	
	//DetachThing(Child);
}

void ACollector::SpawnDirtyPlate()
{
	UE_LOG(LogTemp, Warning, TEXT("SpawnDirtyPlate Null"));
	ActivePlate->ToggleHidden(false);
	DirtyPlateMachine->AddThingOnTop(ActivePlate);
	ActivePlate->ChangeParent(this);
	ActivePlates.Add(ActivePlate);
}

ABaseThing* ACollector::CreateThing()
{
	if(ActivePlates.Num() > 0)
	{
		auto Tmp = ActivePlates[ActivePlates.Num() - 1];
		ActivePlates.RemoveAt(ActivePlates.Num() - 1);
		return Tmp;
	}
	return nullptr;
}
